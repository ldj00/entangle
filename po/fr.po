# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Damien Gourbiere <dgourbiere@gmail.com>, 2012
# Jérôme Fenal <jfenal@gmail.com>, 2012-2014
msgid ""
msgstr ""
"Project-Id-Version: entangle\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-10 21:32+0100\n"
"PO-Revision-Date: 2014-12-22 02:35-0500\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: French (http://www.transifex.com/projects/p/entangle/language/"
"fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Zanata 3.9.6\n"

#: src/backend/entangle-camera-list.c:249 src/backend/entangle-camera.c:719
#, c-format
msgid "Cannot initialize gphoto2 abilities"
msgstr "Impossible d'initialiser les fonctionnalités de gphoto2"

#: src/backend/entangle-camera-list.c:252 src/backend/entangle-camera.c:725
#, c-format
msgid "Cannot load gphoto2 abilities"
msgstr "Impossible de charger les fonctionnalités de gphoto2"

#: src/backend/entangle-camera-list.c:398 src/backend/entangle-camera.c:731
#, c-format
msgid "Cannot initialize gphoto2 ports"
msgstr "Impossible d'initialiser les ports de gphoto2"

#: src/backend/entangle-camera-list.c:401 src/backend/entangle-camera.c:737
#, c-format
msgid "Cannot load gphoto2 ports"
msgstr "Impossible de charger les ports de gphoto2"

#: src/backend/entangle-camera.c:768
#, c-format
msgid "Unable to initialize camera: %s"
msgstr ""

#: src/backend/entangle-camera.c:1122
#, c-format
msgid "Cannot capture image while not connected"
msgstr "Impossible de capture une image sans connexion à l'appareil"

#: src/backend/entangle-camera.c:1135
#, c-format
msgid "Unable to capture image: %s"
msgstr "Impossible de capture une image : %s"

#: src/backend/entangle-camera.c:1249
#, c-format
msgid "Cannot preview image while not connected"
msgstr "Impossible de prévisualiser une image sans connexion à l'appareil"

#: src/backend/entangle-camera.c:1264
#, c-format
msgid "Unable to capture preview: %s"
msgstr "Impossible de capturer une prévisualisation : %s"

#: src/backend/entangle-camera.c:1271 src/backend/entangle-camera.c:1429
#, c-format
msgid "Unable to get file data: %s"
msgstr "Impossible d'obtenir les données du fichier : %s"

#: src/backend/entangle-camera.c:1277
#, c-format
msgid "Unable to get filename: %s"
msgstr "Impossible d'obtenir le nom du fichier : %s"

#: src/backend/entangle-camera.c:1399
#, c-format
msgid "Cannot download file while not connected"
msgstr "Impossible de télécharger une image sans connexion à l'appareil"

#: src/backend/entangle-camera.c:1422
#, c-format
msgid "Unable to get camera file: %s"
msgstr "Impossible d'obtenir le fichier de l'appareil : %s"

#: src/backend/entangle-camera.c:1549
#, c-format
msgid "Cannot delete file while not connected"
msgstr "Impossible de supprimer un fichier sans connexion à l'appareil"

#: src/backend/entangle-camera.c:1567
#, c-format
msgid "Unable to delete file: %s"
msgstr "Impossible de supprimer un fichier : %s"

#: src/backend/entangle-camera.c:1683
#, c-format
msgid "Cannot wait for events while not connected"
msgstr "Impossible d'attendre des événements sans connexion à l'appareil"

#: src/backend/entangle-camera.c:1709
#, c-format
msgid "Unable to wait for events: %s"
msgstr "Impossible d'attendre des événements : %s"

#: src/backend/entangle-camera.c:1904 src/backend/entangle-camera.c:2067
#: src/backend/entangle-camera.c:2203 src/backend/entangle-camera.c:2658
#: src/backend/entangle-camera.c:2833 src/backend/entangle-camera.c:2994
#: src/backend/entangle-camera.c:3218 src/backend/entangle-camera.c:3384
#, c-format
msgid "Unable to fetch widget type"
msgstr "Impossible d'obtenir le type de widget"

#: src/backend/entangle-camera.c:1910 src/backend/entangle-camera.c:2073
#: src/backend/entangle-camera.c:2209
#, c-format
msgid "Unable to fetch widget name"
msgstr "Impossible d'obtenir le nom du widget"

#: src/backend/entangle-camera.c:2322
#, c-format
msgid "Unable to load controls, camera is not connected"
msgstr "Impossible de charger les contrôles, l'appareil n'est pas connecté"

#: src/backend/entangle-camera.c:2331
#, c-format
msgid "Unable to fetch camera control configuration: %s"
msgstr ""

#: src/backend/entangle-camera.c:2452
#, c-format
msgid "Unable to save controls, camera is not connected"
msgstr "Impossible d'enregistrer les contrôles, l'appareil n'est pas connecté"

#: src/backend/entangle-camera.c:2458
#, c-format
msgid "Unable to save controls, camera is not configurable"
msgstr ""
"Impossible d'enregistrer les contrôles, l'appareil n'est pas configurable"

#: src/backend/entangle-camera.c:2477 src/backend/entangle-camera.c:2694
#: src/backend/entangle-camera.c:2863 src/backend/entangle-camera.c:3054
#: src/backend/entangle-camera.c:3240 src/backend/entangle-camera.c:3408
#, c-format
msgid "Unable to save camera control configuration: %s"
msgstr ""

#: src/backend/entangle-camera.c:2580 src/backend/entangle-camera.c:2634
#: src/backend/entangle-camera.c:2812 src/backend/entangle-camera.c:2973
#: src/backend/entangle-camera.c:3197 src/backend/entangle-camera.c:3363
#, c-format
msgid "Controls not available when camera is disconnected"
msgstr ""
"Les contrôles ne sont pas disponibles lorsque l'appareil est déconnecté"

#: src/backend/entangle-camera.c:2586 src/backend/entangle-camera.c:2640
#: src/backend/entangle-camera.c:2818 src/backend/entangle-camera.c:2979
#: src/backend/entangle-camera.c:3203 src/backend/entangle-camera.c:3369
#, c-format
msgid "Controls not available for this camera"
msgstr "Aucun contrôle disponible pour cet appareil"

#: src/backend/entangle-camera.c:2651
#, c-format
msgid "Viewfinder control not available with this camera"
msgstr "Le contrôle du viseur n'est pas disponible pour cet appareil"

#: src/backend/entangle-camera.c:2664
#, c-format
msgid "Viewfinder control was not a toggle widget"
msgstr "Le contrôle du viseur n'est pas un widget à bascule"

#: src/backend/entangle-camera.c:2678 src/backend/entangle-camera.c:2685
#, c-format
msgid "Failed to set viewfinder state: %s %d"
msgstr "Impossible de définir l'état du viseur : %s %d"

#: src/backend/entangle-camera.c:2826
#, c-format
msgid "Autofocus control not available with this camera"
msgstr "Le contrôle de l'autofocus n'est pas disponible pour cet appareil"

#: src/backend/entangle-camera.c:2839
#, c-format
msgid "Autofocus control was not a toggle widget"
msgstr "Le contrôle de l'autofocus n'est pas un widget à bascule"

#: src/backend/entangle-camera.c:2846 src/backend/entangle-camera.c:2854
#, c-format
msgid "Failed to set autofocus state: %s %d"
msgstr "Impossible de définir l'état de l'autofocus : %s %d"

#: src/backend/entangle-camera.c:2987
#, c-format
msgid "Manual focus control not available with this camera"
msgstr "Le contrôle de la focale n'est pas disponible pour cet appareil"

#: src/backend/entangle-camera.c:3001
#, c-format
msgid "Manual focus control was not a range or radio widget"
msgstr ""

#: src/backend/entangle-camera.c:3020 src/backend/entangle-camera.c:3044
#: src/backend/entangle-camera.c:3063 src/backend/entangle-camera.c:3079
#, c-format
msgid "Failed to set manual focus state: %s %d"
msgstr "Impossible de définir l'état manuel de la focale : %s %d"

#: src/backend/entangle-camera.c:3037 src/backend/entangle-camera.c:3072
#, c-format
msgid "Failed to read manual focus choice %d: %s %d"
msgstr ""

#: src/backend/entangle-camera.c:3211
#, c-format
msgid "Time setting not available with this camera"
msgstr "Le réglage de l'heure n'est pas disponible sur cet appareil"

#: src/backend/entangle-camera.c:3224
#, c-format
msgid "Time setting was not a date widget"
msgstr ""

#: src/backend/entangle-camera.c:3231
#, c-format
msgid "Failed to set time state: %s %d"
msgstr "Impossible de définir l'état de l'horloge : %s %d"

#: src/backend/entangle-camera.c:3377
#, c-format
msgid "Capture target setting not available with this camera"
msgstr ""

#: src/backend/entangle-camera.c:3390
#, c-format
msgid "Time setting was not a choice widget"
msgstr ""

#: src/backend/entangle-camera.c:3399
#, c-format
msgid "Failed to set capture target: %s %d"
msgstr ""

#: src/frontend/entangle-application-menus.ui:7
msgid "_New window"
msgstr ""

#: src/frontend/entangle-application-menus.ui:13
msgid "_Preferences…"
msgstr ""

#: src/frontend/entangle-application-menus.ui:19
msgid "_Supported cameras"
msgstr ""

#: src/frontend/entangle-application-menus.ui:23
msgid "_Keyboard shortcuts"
msgstr ""

#: src/frontend/entangle-application-menus.ui:27
msgid "_Help"
msgstr "Aide"

#: src/frontend/entangle-application-menus.ui:31
msgid "_About"
msgstr ""

#: src/frontend/entangle-application-menus.ui:37
msgid "_Quit"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:6
msgid "_Select session…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:12
msgid "_Connect…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:16
msgid "_Disconnect…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:22
msgid "_Close"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:31
msgid "_Fullscreen"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:35
msgid "_Presentation"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:41
msgid "_Settings"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:47
msgid "_Synchronize capture"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:56
msgid "Best Fit"
msgstr "Ajuster"

#: src/frontend/entangle-camera-manager-menus.ui:60
msgid "_Normal Size"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:66
msgid "Zoom _In"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:70
msgid "Zoom _Out"
msgstr ""

#: src/frontend/entangle-camera-manager.c:580
#: src/frontend/entangle-camera-manager.c:584
msgid "Capture an image"
msgstr "Capturer une image"

#: src/frontend/entangle-camera-manager.c:582
#: src/frontend/entangle-camera-manager.c:586
msgid "Continuous capture preview"
msgstr "Prévisualisation de la capture continue"

#: src/frontend/entangle-camera-manager.c:591
#: src/frontend/entangle-camera-manager.c:593
msgid "This camera does not support image capture"
msgstr "Cet appareil ne prend pas en charge la capture d'image"

#: src/frontend/entangle-camera-manager.c:598
#: src/frontend/entangle-camera-manager.c:600
msgid "This camera does not support image preview"
msgstr "Cet appareil ne prend pas en charge la prévisualisation d'image"

#: src/frontend/entangle-camera-manager.c:741
#, c-format
msgid "Operation: %s"
msgstr "Opération : %s"

#: src/frontend/entangle-camera-manager.c:743
msgid "Entangle: Operation failed"
msgstr "Entangle : échec de l'opération"

#: src/frontend/entangle-camera-manager.c:775
#: src/frontend/entangle-camera-manager.c:1038
msgid "Camera load controls failed"
msgstr "Le chargement des contrôles de l'appareil a échoué"

#: src/frontend/entangle-camera-manager.c:777
#: src/frontend/entangle-camera-manager.c:1040
msgid "Entangle: Camera load controls failed"
msgstr "Entangle : Le chargement des contrôles de l'appareil a échoué"

#: src/frontend/entangle-camera-manager.c:815
msgid "Monitor"
msgstr "Moniteur"

#: src/frontend/entangle-camera-manager.c:1000
#: src/frontend/entangle-control-panel.c:841
msgid "No camera connected"
msgstr "Aucun appareil connecté"

#: src/frontend/entangle-camera-manager.c:1089
#: src/frontend/entangle-camera-manager.c:2283
#, c-format
msgid "Unable to connect to camera: %s"
msgstr "Impossible de se connecter à l'appareil : %s"

#: src/frontend/entangle-camera-manager.c:1094
#: src/frontend/entangle-camera-manager.c:2289
msgid ""
"Check that the camera is not\n"
"\n"
" - opened by another photo <b>application</b>\n"
" - mounted as a <b>filesystem</b> on the desktop\n"
" - in <b>sleep mode</b> to save battery power\n"
msgstr ""
"Vérifier que l'appareil photo n'est pas :\n"
"\n"
" - utilisé par une autre <b>application</b> photo,\n"
" - monté en tant que <b>système de fichier</b> sur le bureau,\n"
" - en <b>mode veille</b> afin d'économiser la batterie.\n"

#: src/frontend/entangle-camera-manager.c:1099
#: src/frontend/entangle-camera-manager.c:2294
msgid "Cancel"
msgstr "Annuler"

#: src/frontend/entangle-camera-manager.c:1100
#: src/frontend/entangle-camera-manager.c:2295
msgid "Retry"
msgstr "Réessayer"

#: src/frontend/entangle-camera-manager.c:1131
msgid "Camera is in use"
msgstr "L'appareil est déjà utilisé"

#: src/frontend/entangle-camera-manager.c:1135
msgid ""
"The camera cannot be opened because it is currently mounted as a filesystem. "
"Do you wish to umount it now ?"
msgstr ""
"L'appareil photo ne peut être utilisé car il est actuellement monté en tant "
"que système de fichier. Souhaitez-vous le démonter maintenant ?"

#: src/frontend/entangle-camera-manager.c:1139
#: src/frontend/entangle-camera-picker.c:127
msgid "No"
msgstr "Non"

#: src/frontend/entangle-camera-manager.c:1140
#: src/frontend/entangle-camera-picker.c:127
msgid "Yes"
msgstr "Oui"

#: src/frontend/entangle-camera-manager.c:1175
msgid "Camera connect failed"
msgstr "Échec de la connexion à l'appareil"

#: src/frontend/entangle-camera-manager.c:1177
msgid "Entangle: Camera connect failed"
msgstr "Entangle : échec de la connexion à l'appareil"

#: src/frontend/entangle-camera-manager.c:1487
msgid "Select a folder"
msgstr "Sélectionner un dossier"

#: src/frontend/entangle-camera-manager.c:1490
msgid "_Cancel"
msgstr "_Annuler"

#: src/frontend/entangle-camera-manager.c:1492
msgid "_Open"
msgstr "_Ouvrir"

#: src/frontend/entangle-camera-manager.c:1643
#: src/frontend/entangle-camera-picker.c:531
#: src/frontend/entangle-preferences-display.c:1052
#: src/frontend/entangle-preferences-display.ui:237
msgid "Capture"
msgstr "Capturer"

#: src/frontend/entangle-camera-manager.c:1664
msgid "Script"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1731
msgid "Preview"
msgstr "Prévisualiser"

#: src/frontend/entangle-camera-manager.c:1845
msgid "Set clock"
msgstr "Régler l'heure"

#: src/frontend/entangle-camera-manager.c:1890
msgid "Autofocus failed"
msgstr "Échec de l'autofocus"

#: src/frontend/entangle-camera-manager.c:1892
msgid "Entangle: Camera autofocus failed"
msgstr "Entangle : échec de l'autofocus de l'appareil"

#: src/frontend/entangle-camera-manager.c:1922
msgid "Manual focus failed"
msgstr "Échec du changement manuel de focale"

#: src/frontend/entangle-camera-manager.c:1924
msgid "Entangle: Camera manual focus failed"
msgstr "Entangle : Échec du changement manuel de focale de l'appareil"

#: src/frontend/entangle-camera-manager.c:2418
#: src/frontend/entangle-camera-manager.c:2536
msgid "Delete"
msgstr "Supprimer"

#: src/frontend/entangle-camera-manager.c:2528
msgid "Open"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2532
msgid "Open with"
msgstr "Ouvrir avec"

#: src/frontend/entangle-camera-manager.c:2566
msgid "Select application..."
msgstr "Sélectionner une application..."

#: src/frontend/entangle-camera-manager.c:2905
msgid "Image histogram"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2907
msgid "Automation"
msgstr ""

#: src/frontend/entangle-camera-picker.c:529
msgid "Model"
msgstr "Modèle"

#: src/frontend/entangle-camera-picker.c:530
msgid "Port"
msgstr "Port"

#: src/frontend/entangle-camera-picker.ui:8
msgid "Select camera - Entangle"
msgstr "Sélectionner l'appareil - Entangle"

#: src/frontend/entangle-camera-picker.ui:97
msgid "Select a camera to connect to:"
msgstr "Sélectionner l'appareil auquel se connecter :"

#: src/frontend/entangle-camera-picker.ui:157
msgid ""
"No cameras were detected, check that\n"
"\n"
"  - the <b>cables</b> are connected\n"
"  - the camera is <b>turned on</b>\n"
"  - the camera is in the <b>correct mode</b>\n"
"  - the camera is a <b>supported</b> model\n"
"\n"
"USB cameras are automatically detected\n"
"when plugged in, for others try a refresh\n"
"or enter a network camera IP address"
msgstr ""

#: src/frontend/entangle-camera-picker.ui:198
msgid "IP Address:"
msgstr ""

#: src/frontend/entangle-camera-picker.ui:226
msgid "Network camera"
msgstr ""

#: src/frontend/entangle-camera-support.c:188
msgid "capture"
msgstr "capturer"

#: src/frontend/entangle-camera-support.c:194
msgid "preview"
msgstr "prévisualiser"

#: src/frontend/entangle-camera-support.c:200
msgid "settings"
msgstr "réglages"

#: src/frontend/entangle-camera-support.ui:65
msgid "label"
msgstr "étiquette"

#: src/frontend/entangle-control-panel.c:84
msgid "Camera control update failed"
msgstr "La mise à jour des contrôles de l'appareil a échoué"

#: src/frontend/entangle-control-panel.c:86
msgid "Entangle: Camera control update failed"
msgstr "Entangle : la mise à jour des contrôles de l'appareil a échoué"

#: src/frontend/entangle-control-panel.c:314
msgid "On"
msgstr "Allumé"

#: src/frontend/entangle-control-panel.c:314
msgid "Off"
msgstr "Éteint"

#: src/frontend/entangle-control-panel.c:769
msgid "Reset controls"
msgstr ""

#: src/frontend/entangle-control-panel.c:848
msgid "No controls available"
msgstr "Aucun contrôle disponible"

#: src/frontend/entangle-dpms.c:57
msgid "Screen blanking is not available on this display"
msgstr "L'extinction de l'affichage n'est pas disponible sur cet écran"

#: src/frontend/entangle-dpms.c:69
msgid "Screen blanking is not implemented on this platform"
msgstr "L'extinction de l'affichage n'est pas disponible sur cette plate-forme"

#: src/frontend/entangle-help-about.ui:8
msgid "About - Entangle"
msgstr "À propos - Entangle"

#: src/frontend/entangle-help-about.ui:17 src/entangle.desktop.in:5
msgid "Tethered Camera Control & Capture"
msgstr "Contrôle d'appareil photo et capture asservis"

#: src/frontend/entangle-help-overlay.ui:13
msgctxt "shortcut window"
msgid "Controlling the application"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:17
msgctxt "shortcut window"
msgid "Open a new camera window"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:24
msgctxt "shortcut window"
msgid "Close all windows & exit"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:31
msgctxt "shortcut window"
msgid "Display help manual"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:40
msgctxt "shortcut window"
msgid "Image display"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:44
msgctxt "shortcut window"
msgid "Zoom in"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:51
msgctxt "shortcut window"
msgid "Zoom out"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:58
msgctxt "shortcut window"
msgid "Zoom normal"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:65
msgctxt "shortcut window"
msgid "Zoom best"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:74
msgctxt "shortcut window"
msgid "Window display"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:78
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:85
msgctxt "shortcut window"
msgid "Presentation"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:94
msgctxt "shortcut window"
msgid "Camera control"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:98
msgctxt "shortcut window"
msgid "Capture a single image"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:105
msgctxt "shortcut window"
msgid "Toggle live preview"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1038
#: src/frontend/entangle-preferences-display.ui:645
msgid "Interface"
msgstr "Interface"

#: src/frontend/entangle-preferences-display.c:1045
#: src/frontend/entangle-preferences-display.ui:951
msgid "Image Viewer"
msgstr "Afficheur d'images"

#: src/frontend/entangle-preferences-display.c:1059
#: src/frontend/entangle-preferences-display.ui:446
msgid "Color Management"
msgstr "Gestion de la couleur"

#: src/frontend/entangle-preferences-display.c:1066
#: src/frontend/entangle-preferences-display.ui:516
msgid "Plugins"
msgstr "Greffons"

#: src/frontend/entangle-preferences-display.c:1086
msgid "ICC profiles (*.icc, *.icm)"
msgstr "Profils ICC (*.icc, *.icm)"

#: src/frontend/entangle-preferences-display.c:1091
msgid "All files (*.*)"
msgstr "Tous les fichiers (*.*)"

#: src/frontend/entangle-preferences-display.c:1114
msgid "1:1 - Square / MF 6x6"
msgstr "1:1 - Carré / MF 6x6"

#: src/frontend/entangle-preferences-display.c:1119
msgid "1.15:1 - Movietone"
msgstr "1.15:1 - Movietone"

#: src/frontend/entangle-preferences-display.c:1124
msgid "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"
msgstr "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"

#: src/frontend/entangle-preferences-display.c:1129
msgid "1.37:1 - 35mm movie"
msgstr "1.37:1 - 35mm cinéma"

#: src/frontend/entangle-preferences-display.c:1134
msgid "1.44:1 - IMAX"
msgstr "1.44:1 - IMAX"

#: src/frontend/entangle-preferences-display.c:1139
msgid "1.50:1 (3:2, 15:10)- 35mm SLR"
msgstr "1.50:1 (3:2, 15:10)- 35mm SLR"

#: src/frontend/entangle-preferences-display.c:1144
msgid "1.6:1 (8:5, 16:10) - Widescreen"
msgstr "1.6:1 (8:5, 16:10) - Écran large"

#: src/frontend/entangle-preferences-display.c:1149
msgid "1.66:1 (5:3, 15:9) - Super 16mm"
msgstr "1.66:1 (5:3, 15:9) - Super 16mm"

#: src/frontend/entangle-preferences-display.c:1154
msgid "1.75:1 (7:4) - Widescreen"
msgstr "1.75:1 (7:4) - Écran large"

#: src/frontend/entangle-preferences-display.c:1159
msgid "1.77:1 (16:9) - APS-H / HDTV / Widescreen"
msgstr "1.77:1 (16:9) - APS-H / HDTV / Écran large"

#: src/frontend/entangle-preferences-display.c:1164
msgid "1.85:1 - 35mm Widescreen"
msgstr "1.85:1 - 35mm Écran large"

#: src/frontend/entangle-preferences-display.c:1169
msgid "2.00:1 - SuperScope"
msgstr "2.00:1 - SuperScope"

#: src/frontend/entangle-preferences-display.c:1174
msgid "2.10:1 (21:10) - Planned HDTV"
msgstr "2.10:1 (21:10) - Future HDTV"

#: src/frontend/entangle-preferences-display.c:1179
msgid "2.20:1 (11:5, 22:10) - 70mm movie"
msgstr "2.20:1 (11:5, 22:10) - 70mm cinéma"

#: src/frontend/entangle-preferences-display.c:1184
msgid "2.35:1 - CinemaScope"
msgstr "2.35:1 - CinemaScope"

#: src/frontend/entangle-preferences-display.c:1189
msgid "2.37:1 (64:27)- HDTV cinema"
msgstr "2.37:1 (64:27)- HDTV cinéma"

#: src/frontend/entangle-preferences-display.c:1194
msgid "2.39:1 (12:5)- Panavision"
msgstr "2.39:1 (12:5)- Panavision"

#: src/frontend/entangle-preferences-display.c:1199
msgid "2.55:1 (23:9)- CinemaScope 55"
msgstr "2.55:1 (23:9)- CinemaScope 55"

#: src/frontend/entangle-preferences-display.c:1204
msgid "2.59:1 (13:5)- Cinerama"
msgstr "2.59:1 (13:5)- Cinerama"

#: src/frontend/entangle-preferences-display.c:1209
msgid "2.66:1 (8:3, 24:9)- Super 16mm"
msgstr "2.66:1 (8:3, 24:9)- Super 16mm"

#: src/frontend/entangle-preferences-display.c:1214
msgid "2.76:1 (11:4) - Ultra Panavision"
msgstr "2.76:1 (11:4) - Ultra Panavision"

#: src/frontend/entangle-preferences-display.c:1219
msgid "2.93:1 - MGM Camera 65"
msgstr "2.93:1 - MGM Camera 65"

#: src/frontend/entangle-preferences-display.c:1224
msgid "3:1 APS Panorama"
msgstr "3:1 APS Panorama"

#: src/frontend/entangle-preferences-display.c:1229
msgid "4.00:1 - Polyvision"
msgstr "4.00:1 - Polyvision"

#: src/frontend/entangle-preferences-display.c:1234
msgid "12.00:1 - Circle-Vision 360"
msgstr "12.00:1 - Circle-Vision 360"

#: src/frontend/entangle-preferences-display.c:1251
msgid "None"
msgstr "Aucun"

#: src/frontend/entangle-preferences-display.c:1256
msgid "Center lines"
msgstr "Centrage des lignes"

#: src/frontend/entangle-preferences-display.c:1261
msgid "Rule of 3rds"
msgstr "Règle des tiers"

#: src/frontend/entangle-preferences-display.c:1266
msgid "Quarters"
msgstr "Quarts"

#: src/frontend/entangle-preferences-display.c:1271
msgid "Rule of 5ths"
msgstr "Règles des 5emes"

#: src/frontend/entangle-preferences-display.c:1276
msgid "Golden sections"
msgstr "Sections dorées"

#: src/frontend/entangle-preferences-display.ui:8
msgid "Entangle Preferences"
msgstr "Préférences de Entangle"

#: src/frontend/entangle-preferences-display.ui:101
msgid "<b>Capture</b>"
msgstr "<b>Capturer</b>"

#: src/frontend/entangle-preferences-display.ui:145
msgid "Filename pattern:"
msgstr "Motif de nom de fichier :"

#: src/frontend/entangle-preferences-display.ui:163
msgid "Continue preview mode after capture"
msgstr "Continuer le mode prévisualisation après capture"

#: src/frontend/entangle-preferences-display.ui:179
msgid "Use preview output as capture image"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:195
msgid "Delete file from camera after downloading"
msgstr "Supprimer le fichier après téléchargement"

#: src/frontend/entangle-preferences-display.ui:211
msgid "Automatically synchronize camera clock"
msgstr "Réglage automatique de l'horloge de l'appareil"

#: src/frontend/entangle-preferences-display.ui:263
msgid "<b>Colour management</b>"
msgstr "<b>Gestion de la couleur</b>"

#: src/frontend/entangle-preferences-display.ui:310
msgid "Mode of operation:"
msgstr "Mode d'opération"

#: src/frontend/entangle-preferences-display.ui:318
msgid "RGB profile:"
msgstr "Profil RVB :"

#: src/frontend/entangle-preferences-display.ui:330
msgid "Monitor profile:"
msgstr "Profil du moniteur :"

#: src/frontend/entangle-preferences-display.ui:342
msgid "Rendering intent:"
msgstr "Intention de rendu :"

#: src/frontend/entangle-preferences-display.ui:352
msgid "Detect the system monitor profile"
msgstr "Détecter le profil du moniteur système"

#: src/frontend/entangle-preferences-display.ui:417
msgid "Colour managed display"
msgstr "Affichage aux couleurs administrées"

#: src/frontend/entangle-preferences-display.ui:473
msgid "<b>Plugins</b>"
msgstr "<b>Greffons</b>"

#: src/frontend/entangle-preferences-display.ui:543
msgid "<b>Interface</b>"
msgstr "<b>Interface</b>"

#: src/frontend/entangle-preferences-display.ui:585
msgid "Automatically connect to cameras at startup"
msgstr "Se connecter automatiquement aux appareils au démarrage"

#: src/frontend/entangle-preferences-display.ui:599
msgid "Blank screen when capturing images"
msgstr "Effacer l'écran lors de la capture des images"

#: src/frontend/entangle-preferences-display.ui:615
msgid "Show linear histogram"
msgstr "Afficher l'histogramme linéaire"

#: src/frontend/entangle-preferences-display.ui:672
msgid "<b>Image Viewer</b>"
msgstr "<b>Afficheur d'images</b>"

#: src/frontend/entangle-preferences-display.ui:714
msgid "Apply mask to alter aspect ratio"
msgstr "Appliquer le masque pour modifier le ratio"

#: src/frontend/entangle-preferences-display.ui:730
msgid "Aspect ratio:"
msgstr "Rapport d'aspect :"

#: src/frontend/entangle-preferences-display.ui:742
msgid "Mask opacity:"
msgstr "Opacité du masque :"

#: src/frontend/entangle-preferences-display.ui:769
msgid "0"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:782
msgid "Display focus point during preview"
msgstr "Afficher le point de focalisation pendant la prévisualisation"

#: src/frontend/entangle-preferences-display.ui:800
msgid "Grid lines:"
msgstr "Quadrillage :"

#: src/frontend/entangle-preferences-display.ui:824
msgid "Use embedded preview from raw files"
msgstr "Utiliser les vignettes de prévisualisation des fichiers raw"

#: src/frontend/entangle-preferences-display.ui:840
msgid "Overlay earlier images"
msgstr "Ajouter une surcouche à des images précédentes"

#: src/frontend/entangle-preferences-display.ui:858
msgid "Overlay layers:"
msgstr "Surcouches :"

#: src/frontend/entangle-preferences-display.ui:871
msgid "3"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:887
msgid "Background:"
msgstr "Fond :"

#: src/frontend/entangle-preferences-display.ui:913
msgid "Highlight:"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:1000
msgid "Perceptual"
msgstr "Perceptif"

#: src/frontend/entangle-preferences-display.ui:1003
msgid "Relative colourimetric"
msgstr "Colorimétrie relative"

#: src/frontend/entangle-preferences-display.ui:1006
msgid "Saturation"
msgstr "Saturation"

#: src/frontend/entangle-preferences-display.ui:1009
msgid "Absolute colourimetric"
msgstr "Colorimétrie absolue"

#: src/frontend/entangle-script.c:96
msgid "Untitled script"
msgstr ""

#: src/frontend/entangle-script-config.c:79
msgid "No script"
msgstr ""

#: src/frontend/entangle-script-config.c:161
msgid "No config options"
msgstr ""

#: src/frontend/entangle-script-simple.c:63
msgid "Missing 'execute' method implementation"
msgstr ""

#: src/entangle.appdata.xml.in:7 src/entangle.desktop.in:4
msgid "Entangle"
msgstr "Entangle"

#: src/entangle.appdata.xml.in:8
msgid "Tethered Camera Control &amp; Capture"
msgstr "Pilotage de contrôle et de capture par appareil photo"

#: src/entangle.appdata.xml.in:10
msgid ""
"Entangle is a program used to control digital cameras that are connected to "
"the computer via USB."
msgstr ""
"Entangle permet de contrôler les appareils photo numériques connecté à "
"l'ordinateur via USB."

#: src/entangle.appdata.xml.in:14
msgid ""
"Entangle can trigger the camera shutter to capture new images. When "
"supported by the camera, a continuously updating preview of the scene can be "
"displayed prior to capture. Images will be downloaded and displayed as they "
"are captured by the camera. Entangle also allows the settings of the camera "
"to be changed from the computer."
msgstr ""
"Entangle peut déclencher l'obturateur de l'appareil afin de capturer de "
"nouvelles images. Lorsque l'appareil le permet, une prévisualisation "
"continue de la scène peut être affichée avant la capture. Les images sont "
"téléchargées et affichées lorsqu'elles sont capturées par l'appareil. "
"Entangle permet aussi de définir les réglages de l'appareil depuis "
"l'ordinateur."

#: src/entangle.appdata.xml.in:22
msgid ""
"Entangle is compatible with most DSLR cameras from Nikon and Canon, some of "
"their compact camera models, and a variety of cameras from other "
"manufacturers."
msgstr ""
"Entangle est compatible avec la plupart des appareils photographiques reflex "
"numériques, certains appareils compacts, ainsi qu'un certain nombre "
"d'appareils d'autres fabricants."

#: src/entangle.desktop.in:6
msgid "entangle"
msgstr ""

#: src/entangle.desktop.in:12
msgid "Capture;Camera;Tethered;Photo;"
msgstr ""
